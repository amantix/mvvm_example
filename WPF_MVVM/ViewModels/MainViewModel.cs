﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WPF_MVVM.Annotations;
using WPF_MVVM.Models;

namespace WPF_MVVM.ViewModels
{
    public class MainViewModel: INotifyPropertyChanged
    {
        public ObservableCollection<Note> Goals { get; set; }

        private Project _project;
        public MainViewModel(Project project)
        {
            _project = project;
            Goals = new ObservableCollection<Note>(project.Goals);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}