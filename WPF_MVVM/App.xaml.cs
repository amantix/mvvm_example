﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF_MVVM.Models;
using WPF_MVVM.ViewModels;

namespace WPF_MVVM
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var view = new MainWindow();
            

            Project project;// = new Project();
            //project.Goals.Add(new Note("a", "aa"));
            //project.Goals.Add(new Note("c", "cc"));


            //using (var sw = new StreamWriter("output.xml"))
            //{
            //    var xs = new System.Xml.Serialization.XmlSerializer(typeof(Project));  
            //    xs.Serialize(sw, project);
            //}

            using (var sr = new StreamReader("output.xml"))
            {
                var xs = new System.Xml.Serialization.XmlSerializer(typeof(Project));
                project=(Project)xs.Deserialize(sr);
            }

            var viewModel = new MainViewModel(project);

            view.DataContext = viewModel;
            view.Show();
        }
    }
}
